import { API_URL, CoinChart, CoinStats } from '../entities';

function handleError(response) {
  if (response?.error) {
    throw new Error(response?.error);
  }

  return response;
}

export abstract class Api {
  static async getCoinStatsForDate(
    coin: string,
    date: string,
  ): Promise<CoinStats> {
    return fetch(
      `${API_URL}/coins/${coin}/history?` +
        new URLSearchParams({
          date,
        }),
      {
        method: 'GET',
      },
    ).then((response) => response.json()).then(handleError);
  }

  static async getCoinStats(coin: string): Promise<CoinStats> {
    return fetch(`${API_URL}/coins/${coin}`, {
      method: 'GET',
    }).then((response) => response.json()).then(handleError);
  }

  static async getCoinChart(
    coin: string,
    from: number,
    to: number,
  ): Promise<CoinChart> {
    return fetch(
      `${API_URL}/coins/${coin}/market_chart/range?` +
        new URLSearchParams({
          from: from.toString(),
          to: to.toString(),
          vs_currency: 'USD',
        }),
      {
        method: 'GET',
      },
    ).then((response) => response.json()).then(handleError);
  }
}
