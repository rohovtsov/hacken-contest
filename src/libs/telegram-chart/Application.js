import {ApplicationInstance} from "./ApplicationInstance";
import {Utils} from "./Utils";

class Application {
    constructor(chartDatas) {
        this.applicationInstances_ = [];

        this.prevDrawingTimestamp_ = Date.now();
        this.themeAnimationSpeed_ = -1 / 200;
        this.themeAnimationPercent_ = 0;
        this.processedThemeAnimationPercent_ = false;
        this.chartsThemeSwitcherButton_ = document.querySelector('.charts-theme-switcher-button');

        this.init_(chartDatas);
        this.attachEvents_();
        this.redraw_();
    }

    init_(chartDatas) {
        var applicationElements = document.querySelectorAll('.charts-application');
        for (let id = 0; id < applicationElements.length; id++) {
            this.applicationInstances_.push(new ApplicationInstance(applicationElements[id], chartDatas[id], chartDatas[Math.min(id + 1, applicationElements.length - 1)]))
        }

        if (document.body.classList.contains('charts-theme-night')) {
            this.themeAnimationSpeed_ = 1 / 200;
            this.themeAnimationPercent_ = 1;
        }
    }

    attachEvents_() {
        let minimumResizeIntervalBeforeRecalculations = 100;
        let lastResizeAt = -minimumResizeIntervalBeforeRecalculations;
        window.addEventListener('resize', function (event) {
            if (event.timeStamp - lastResizeAt > minimumResizeIntervalBeforeRecalculations) {
                setTimeout(this.resize_.bind(this), minimumResizeIntervalBeforeRecalculations);
                lastResizeAt = event.timeStamp;
            }
        }.bind(this));

        let timestampDeltaToFocusAppear = 60;
        let clickedTimestamp = -timestampDeltaToFocusAppear;
        this.chartsThemeSwitcherButton_.addEventListener('click', function (event) {
            clickedTimestamp = event.timeStamp;
            this.themeAnimationSpeed_ = -this.themeAnimationSpeed_;
        }.bind(this));
        this.chartsThemeSwitcherButton_.addEventListener('mousedown', function (event) {
            clickedTimestamp = event.timeStamp;
        }.bind(this));
        this.chartsThemeSwitcherButton_.addEventListener('focus', function (event) {
            if (Math.abs(event.timeStamp - clickedTimestamp) > timestampDeltaToFocusAppear) {
                this.chartsThemeSwitcherButton_.classList.add('charts-focus');
            }
        }.bind(this));
        this.chartsThemeSwitcherButton_.addEventListener('blur', function () {
            this.chartsThemeSwitcherButton_.classList.remove('charts-focus');
        }.bind(this));
    }

    resize_() {
        for(let applicationId in this.applicationInstances_) {
            this.applicationInstances_[applicationId].resize();
        }
    }

    processThemeAnimation_(timePastInMs) {
        let bodyClassList = document.body.classList;

        this.themeAnimationPercent_ = Utils.clamp(this.themeAnimationPercent_ + this.themeAnimationSpeed_ * timePastInMs, 0, 1);
        if (this.themeAnimationPercent_ === this.processedThemeAnimationPercent_) {
            return;
        }

        this.processedThemeAnimationPercent_ = this.themeAnimationPercent_;

        bodyClassList.toggle('charts-theme-night', (
            this.themeAnimationPercent_ !== 0 &&
            this.themeAnimationSpeed_ > 0
        ));
        bodyClassList.toggle('charts-theme-animating', (
            this.themeAnimationPercent_ !== 0 &&
            this.themeAnimationPercent_ !== 1
        ));

        for(let applicationId in this.applicationInstances_) {
            this.applicationInstances_[applicationId].themeChange(this.themeAnimationPercent_);
        }
    }

    redraw_() {
        window.requestAnimationFrame(this.redraw_.bind(this));

        let nowTimestamp = Date.now();
        let timePastInMs = nowTimestamp - this.prevDrawingTimestamp_;
        this.prevDrawingTimestamp_ = nowTimestamp;

        this.processThemeAnimation_(timePastInMs);

        for(let applicationId in this.applicationInstances_) {
            this.applicationInstances_[applicationId].redraw(timePastInMs);
        }
    }
}
