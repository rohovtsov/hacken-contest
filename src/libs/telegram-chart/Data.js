export const getChartData = (type, x, y) => {
  const dataJson = JSON.stringify({
    columns: [['x', ...x], ...y.map((yArr, id) => [`y${id}`, ...yArr])],
    types: {
      y0: '%type%',
      y1: '%type%',
      y2: '%type%',
      y3: '%type%',
      y4: '%type%',
      y5: '%type%',
      y6: '%type%',
      x: 'x',
    },
    names: {
      y0: 'Apples',
      y1: 'Oranges',
      y2: 'Lemons',
      y3: 'Apricots',
      y4: 'Kiwi',
      y5: 'Mango',
      y6: 'Pears',
    },
    colors: {
      y0: '#3497ED',
      y1: '#2373DB',
      y2: '#9ED448',
      y3: '#5FB641',
      y4: '#F5BD25',
      y5: '#F79E39',
      y6: '#E65850',
    },
    stacked: true,
  });
  const chartDataObject = JSON.parse(dataJson.split('%type%').join(type));

  return minifyTelegramChartData(chartDataObject);
};

const minifyTelegramChartData = (chartData) => {
  const graphs = [];
  let type = null;

  for (let i = 1; i < chartData.columns.length; i++) {
    const points = [];

    for (let j = 1; j < chartData.columns[i].length; j++) {
      points.push(chartData.columns[i][j]);
    }

    if (type === null) {
      type = chartData.types['y' + (i - 1)];
      if (chartData.y_scaled) {
        type = 'line2y';
      }
    }

    graphs.push({
      data: points,
      color: chartData.colors['y' + (i - 1)],
      label: chartData.names['y' + (i - 1)],
    });
  }

  return {
    time: chartData.columns[0][1],
    interval: chartData.columns[0][2] - chartData.columns[0][1],
    graphs,
    type,
  };
};
