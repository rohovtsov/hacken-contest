import {Utils} from "./Utils";

export class RangeControl {
    constructor(model, root) {
        this.model_ = model;
        this.rangeFrom = 0;
        this.rangeTo = 0.25;
        /*this.root_ = root;
        this.rootRect_ = this.root_.getBoundingClientRect();

        this.fromInput_ = this.root_.querySelector('.charts-range-from');
        this.toInput_ = this.root_.querySelector('.charts-range-to');
        this.toInputPrevValue_ = parseFloat(this.toInput_.value);
        this.fromInputPrevValue_ = parseFloat(this.fromInput_.value);
        this.rangeFrom = null;
        this.rangeTo = null;

        this.rangeMinDistance_ = null;
        this.rangeMinWidth_ = 48;//16 * 3;
        this.rangeMaxUnitX_ = 80;

        this.toggleFrom_ = this.root_.querySelector('.charts-range-toggle-from');
        this.toggleTo_ = this.root_.querySelector('.charts-range-toggle-to');
        this.toggleMiddle_ = this.root_.querySelector('.charts-range-toggle-middle');
        this.toggleFromPressed_ = false;
        this.toggleMiddlePressed_ = false;
        this.toggleToPressed_ = false;

        this.toggle_ = this.root_.querySelector('.charts-range-toggle');

        this.calculateRangeMinDistance_();
        this.onRangeValueChanged_();
        this.attachEvents_();*/
    }

    calculateRangeMinDistance_() {
        this.rangeMinDistance_ = Math.max(
            this.rangeMinWidth_ / this.rootRect_.width,
            this.model_.canvasWidth / this.rangeMaxUnitX_ / this.model_.chart.lastPointIndex
        );

        this.rangeMinDistance_ = Math.min(this.rangeMinDistance_, .5);

        this.fromInputPrevValue_ = Utils.clamp(this.fromInputPrevValue_, 0, this.toInputPrevValue_ - this.rangeMinDistance_);
        if (this.fromInputPrevValue_ < 0) {
            this.fromInputPrevValue_ = 0;
            this.toInputPrevValue_ = this.rangeMinDistance_;
        }

        this.toInputPrevValue_ = Utils.clamp(this.toInputPrevValue_, this.fromInputPrevValue_ + this.rangeMinDistance_, 1);
        this.setRangeValues_(this.fromInputPrevValue_, this.toInputPrevValue_);
    }

    onRangeValueChanged_() {

    }

    setRange(fromPercent, toPercent, offset = 0) {
      let rangeFrom = fromPercent;
      let rangeTo = toPercent;

      if (offset > 0) {
        let pxInPercent = (this.model_.canvasWidth - 2 * offset) / (toPercent - fromPercent);
        let boundingOffsetPercent = offset / pxInPercent;
        rangeTo = toPercent + boundingOffsetPercent;
        rangeFrom = fromPercent - boundingOffsetPercent;
        console.log(rangeFrom, rangeTo);
      }

      this.rangeTo = rangeTo;
      this.rangeFrom = rangeFrom;
    }

    resize() {
        //this.onRangeValueChanged_();
    }

    attachEvents_() {
        this.toInput_.addEventListener('input', function () {
            let toValue = parseFloat(this.toInput_.value);
            this.setRangeValues_(this.fromInputPrevValue_, toValue);
        }.bind(this));

        this.fromInput_.addEventListener('input', function () {
            let fromValue = parseFloat(this.fromInput_.value);
            this.setRangeValues_(fromValue, this.toInputPrevValue_);
        }.bind(this));

        window.addEventListener('mousemove', this.onTouchMove_.bind(this));
        window.addEventListener('mousedown', this.onTouchDown_.bind(this));
        window.addEventListener('mouseup', this.onTouchUp_.bind(this));

        window.addEventListener('touchstart', this.onTouchDown_.bind(this));
        window.addEventListener('touchmove', this.onTouchMove_.bind(this));
        window.addEventListener('touchend', this.onTouchUp_.bind(this));
    }

    onTouchMove_(event) {
        let touchX;
        if (event.changedTouches && event.changedTouches.length) {
            touchX = event.changedTouches[event.changedTouches.length - 1].pageX - this.rootRect_.left;
        } else {
            touchX = event.pageX - this.rootRect_.left;
        }

        if (this.toggleFromPressed_ !== false) {
            this.setRangeValues_(
                Utils.clamp(((touchX - this.toggleFromPressed_) / this.rootRect_.width), 0, this.toInputPrevValue_ - this.rangeMinDistance_),
                this.toInputPrevValue_
            );
        } else if (this.toggleToPressed_ !== false) {
            this.setRangeValues_(
                this.fromInputPrevValue_,
                Utils.clamp(((touchX - this.toggleToPressed_) / this.rootRect_.width), this.fromInputPrevValue_ + this.rangeMinDistance_, 1)
            );
        } else if (this.toggleMiddlePressed_ !== false) {
            let from = Utils.clamp(((touchX - this.toggleMiddlePressed_) / this.rootRect_.width), 0, 1);

            let rangeWidth = (this.toInputPrevValue_ - this.fromInputPrevValue_);
            if (rangeWidth < this.rangeMinDistance_) {
                rangeWidth = this.rangeMinDistance_;
            }

            let to = from + rangeWidth;

            if (to > 1) {
                from = 1 - rangeWidth;
                to = 1;
            }

            this.setRangeValues_(
                from,
                to
            );
        }
    }

    onTouchDown_(event) {
        let layerX;
        if (event.changedTouches && event.changedTouches.length) {
            layerX = event.changedTouches[event.changedTouches.length - 1].pageX - event.target.getBoundingClientRect().left;
        } else {
            layerX = event.pageX - event.target.getBoundingClientRect().left;
        }

        //TODO: get value from CSS
        let clickAreaOffsetInPx = 11;

        if (event.target === this.toggleTo_) {
            //TODO: this is a hack
            this.toggleToPressed_ = layerX - this.toggleTo_.getBoundingClientRect().width + clickAreaOffsetInPx;
        } else if (event.target === this.toggleFrom_) {
            this.toggleFromPressed_ = layerX - clickAreaOffsetInPx;
        } else if (event.target === this.toggleMiddle_) {
            this.toggleMiddlePressed_ = layerX;
        }
    }

    onTouchUp_() {
        if (this.toggleToPressed_ !== false) {
            this.toggleToPressed_ = false;
        }
        if (this.toggleFromPressed_ !== false) {
            this.toggleFromPressed_ = false;
        }
        if (this.toggleMiddlePressed_ !== false) {
            this.toggleMiddlePressed_ = false;
        }
    }

    setRangeValues_(fromValue, toValue) {
        let valueChanged = false;

        if (
            fromValue + this.rangeMinDistance_ <= toValue &&
            toValue <= 1 && toValue >= 0 && fromValue >= 0 && fromValue <= 1
        ) {
            if (this.toInputPrevValue_ !== toValue) {
                this.toInput_.value = this.toInputPrevValue_ = toValue;
                valueChanged = true;
            }
            if (this.fromInputPrevValue_ !== fromValue) {
                this.fromInput_.value = this.fromInputPrevValue_ = fromValue;
                valueChanged = true;
            }
        }

        if (valueChanged) {
            this.onRangeValueChanged_();
        }
    }
}
