import {ChartDrawable} from "../drawables/ChartDrawable";
import {ViewModel} from "../basic/ViewModel";

export class View {
    constructor(canvasElement) {
        this.model_ = new ViewModel(canvasElement);
        this.prevChartDrawable_ = this.chartDrawable_ = null;
    }

    setRange(rangeFrom, rangeTo) {
        if (
            this.prevRangeTo_ !== rangeTo ||
            this.prevRangeFrom_ !== rangeFrom
        ) {
            this.model_.setRange(rangeFrom, rangeTo);
            this.prevRangeTo_ = rangeTo;
            this.prevRangeFrom_ = rangeFrom;
        }
    }

    setChart(chart, rangeFrom, rangeTo, opt_animateFactor) {
        this.setRange(rangeFrom, rangeTo);
        this.model_.setChart(chart);
        this.prevChartDrawable_ = this.chartDrawable_;
        this.chartDrawable_ = new ChartDrawable(this.model_, opt_animateFactor);
        this.model_.triggerNextFrameRedraw = true;
    }

    draw(timePastInMs) {
        if (this.shouldDraw_()) {
            this.model_.canvasContext.clearRect(
                0,
                -this.model_.canvasPaddingTop,
                this.model_.canvasWidth,
                this.model_.canvasHeight + this.model_.canvasPaddingTop + this.model_.canvasPaddingBottom
            );

            this.chartDrawable_.preDraw(timePastInMs);

            this.onDraw_(timePastInMs);

            this.chartDrawable_.postDraw(timePastInMs);
        }
    }

    resize() {
        this.model_.calculateCanvasBounds();
    }

    shouldDraw_() {
        if (this.model_.triggerNextFrameRedraw) {
            this.model_.triggerNextFrameRedraw = false;
            return true;
        }

        return this.chartDrawable_.isNeedsToBeRedrawn();
    }

    onDraw_(timePastInMs) {
        this.chartDrawable_.draw(timePastInMs);
    }

    themeChange(themeAnimationPercent){}
}
