import {View} from "./View";
import {RulersDrawable} from "../drawables/RulersDrawable";
import {ChartType} from "../basic/Chart";

export class ViewPreview extends View {
    constructor(canvasElement, drawHorizontalRulers) {
        super(canvasElement);
        this.model_.canvasPaddingTop = 20;
        this.model_.canvasPaddingBottom = 25;
        this.model_.calculateCanvasBounds();

        this.drawHorizontalRulers_ = drawHorizontalRulers;
        this.rulersDrawable_ = new RulersDrawable(this.model_);
        //this.summaryDrawable_ = new SummaryDrawable(this.model_, root.querySelector('.charts-preview canvas'), root.querySelector('.charts-summary'), root.querySelector('.charts-summary-line'));

        this.fadersColorPrefix_ = null;
    }

    setOffsets(topOffset, rulersOffsets, bottomOffset = null) {
      if (
        this.model_.canvasPaddingTop === topOffset &&
        this.rulersDrawable_.rulersXOffset_ === rulersOffsets &&
        (bottomOffset === null || this.model_.canvasPaddingBottom === bottomOffset)
      ) {
        return;
      }

      this.model_.canvasPaddingTop = topOffset;
      this.rulersDrawable_.rulersXOffset_ = rulersOffsets;

      if (bottomOffset !== null) {
        this.model_.canvasPaddingBottom = bottomOffset;
      }

      this.model_.calculateCanvasBounds();
    }

    shouldDraw_() {
        return (
            super.shouldDraw_()
        );
    }

    onDraw_(timePastInMs) {
        let drawRulers = true;
        if (this.model_.chart.type === ChartType.PIE) {
            drawRulers = false;
        }

        //this.summaryDrawable_.findOutPointIndex(this.chartDrawable_.scales);
        super.onDraw_(timePastInMs);

        if (drawRulers ) {
            this.rulersDrawable_.drawHorizontal(
                timePastInMs,
                this.model_.chart,
                this.chartDrawable_.calculations.maxPointValues,
                this.chartDrawable_.calculations.minPointValues,
                this.chartDrawable_.calculations.maxPointValue,
                this.chartDrawable_.calculations.minPointValue,
                this.chartDrawable_.scales
            );
        }
        if (this.model_.chart.type !== ChartType.PIE) {
            this.drawFaders_();
        }

        if (drawRulers && this.drawHorizontalRulers_) {
            this.rulersDrawable_.drawVertical(timePastInMs, this.chartDrawable_.scales, this.model_.chart);
        }
        //this.summaryDrawable_.draw(this.chartDrawable_.scales);
    }

    drawFaders_() {
        this.drawFader_(-this.model_.canvasPaddingTop, false);
        this.drawFader_(this.model_.canvasHeight + this.model_.canvasPaddingBottom, true);
    }

    drawFader_(fromY, reverse) {
        if (!this.fadersColorPrefix_) {
            return;
        }

        let context = this.model_.canvasContext;
        let faderHeight = 20;

        let grd = context.createLinearGradient(0,  fromY + (reverse ? -faderHeight : 0), 0, fromY + (reverse ? -faderHeight : 0) + faderHeight);
        grd.addColorStop(reverse ? 1 : 0,   this.fadersColorPrefix_ + '1)');
        grd.addColorStop(reverse ? 0 : 1, this.fadersColorPrefix_ + '0)');

        context.fillStyle = grd;
        context.fillRect(0, fromY + (reverse ? -faderHeight : 0), this.model_.canvasWidth, faderHeight);
    }

    themeChange(themeAnimationPercent){
        if (themeAnimationPercent === 0) {
            this.fadersColorPrefix_ = 'rgba(255, 255, 255,';
        } else if (themeAnimationPercent === 1) {
            this.fadersColorPrefix_ = 'rgba(36, 47, 62,';
        } else {
            this.fadersColorPrefix_ = null;
        }

        this.rulersDrawable_.changeRulersColor(themeAnimationPercent);
        this.model_.triggerNextFrameRedraw = true;
    }
}
