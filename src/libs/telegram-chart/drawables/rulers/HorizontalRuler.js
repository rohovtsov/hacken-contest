export class HorizontalRuler {
    constructor(model, amount, lineIncluded, labelsRightSide, labelsColor) {
        this.model_ = model;
        this.amount = amount;
        this.progress = 0;
        this.valuesPrev = [];
        this.values = [];
        this.labelValuesPrev = [];
        this.labelValues = [];
        this.lineIncluded = lineIncluded;
        this.labelsRightSide = labelsRightSide;
        this.labelsColor = labelsColor;

        this.rulersLabelYOffset_ = 8;
        this.pxToRem_ = parseFloat(getComputedStyle(document.documentElement).fontSize);

        this.prevMaxValue_ = null;
        this.prevMinValue_ = null;
        this.prevUnitY_ = null;
        this.prevOffsetY_ = null;
    }

    calculateHorizontalRulerValues(maxValue, minValue, scale) {
        let unitYHasToBe = this.model_.canvasHeight / (maxValue - minValue);
        let offsetYHasToBe = - minValue * unitYHasToBe;

        /*if (maxValue === 0 && this.model_.scale.unitY !== 0) {
            maxValue = this.prevMaxValue_;
            minValue = this.prevMinValue_;
        }*/

        if (
            this.prevMaxValue_ == null ||
            this.prevMinValue_ == null
        ) {
            this.prevMaxValue_ = maxValue;
            this.prevMinValue_ = minValue;
            this.prevOffsetY_ = offsetYHasToBe;
            this.prevUnitY_ = unitYHasToBe;
        }

        if (
            this.prevMaxValue_ !== maxValue ||
            this.prevMinValue_ !== minValue
        ) {
            this.prevUnitY_ = this.model_.canvasHeight / (this.prevMaxValue_ - this.prevMinValue_);
            this.prevOffsetY_ = -this.prevMinValue_ * this.prevUnitY_;
            this.valuesPrev = this.values.slice();
            this.labelValuesPrev = this.labelValues.slice();
        }

        this.prevMaxValue_ = maxValue;
        this.prevMinValue_ = minValue;

        let unitYWasToBe = this.prevUnitY_;
        let offsetYWasToBe = this.prevOffsetY_;

        let progress =
            unitYWasToBe - unitYHasToBe > 0 ?
                1 - Math.abs((scale.unitY - unitYHasToBe) / (unitYWasToBe - unitYHasToBe)) :
                Math.abs((scale.unitY - unitYWasToBe) / (unitYHasToBe - unitYWasToBe));

        if (
            isNaN(progress) ||
            !isFinite(progress)
        ) {
            progress = 1;
        }

        this.progress = progress;

        //WHAT DA FUCK IS THIS?!
        /*let progress = this.horizontalRulersProgress_ = Math.abs((this.drawingContext_.canvasHeight / this.drawingContext_.unitY) / (maxValue));
          if (maxValue < this.lastMaxValue_) {
              progress = 1 / progress;
          }*/

        let rulersHeight = this.model_.canvasHeight - .75 * this.pxToRem_ - this.rulersLabelYOffset_;

        for (let i = 0; i < this.amount; i++) {
            let lineValue = Math.round(
                (rulersHeight - (rulersHeight * i / (this.amount - 1))  - offsetYHasToBe) / unitYHasToBe
            );
            let labelValue = Math.round(
                (this.model_.canvasHeight - (this.model_.canvasHeight * i / (this.amount - 1))  - offsetYHasToBe) / unitYHasToBe
            );
            this.values[i] = lineValue;
            this.labelValues[i] = labelValue;
        }
    }
}
