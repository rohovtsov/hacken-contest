import {Utils} from "../Utils";

export class GraphDrawable {
    constructor(model, graph) {
        this.graph = graph;
        this.model_ = model;
        this.rgbPrefix_ = Utils.hexToRgbPrefix(this.graph.color);

        this.opacity_ = this.graph.enabled ? 1 : 0;
        this.maxPointValue = 0;
        this.minPointValue = 0;
    }

    isDrawingContextChanged_(scale) {
        return (
            this.prevOffsetX_ !== scale.offsetX ||
            this.prevOffsetY_ !== scale.offsetY ||
            this.prevUnitX_ !== scale.unitX ||
            this.prevUnitY_ !== scale.unitY ||
            this.prevCanvasWidth_ !== this.model_.canvasWidth ||
            this.prevCanvasHeight_ !== this.model_.canvasHeight
        )
    }

    saveDrawingContext_(scale) {
        this.prevCanvasHeight_ = this.model_.canvasHeight;
        this.prevCanvasWidth_ = this.model_.canvasWidth;
        this.prevUnitY_ = scale.unitY;
        this.prevUnitX_ = scale.unitX;
        this.prevOffsetX_ = scale.offsetX;
        this.prevOffsetY_ = scale.offsetY;
    }

    isOpacityChanged_() {
        return (
            (this.graph.enabled && this.opacity_ !== 1) ||
            (!this.graph.enabled && this.opacity_ !== 0)
        );
    }

    isNeedsToBeRedrawn(scale) {
        return (
            this.isDrawingContextChanged_(scale) ||
            this.isOpacityChanged_()
        );
    }

    processOpacityChange_(timePastInMs){
        let opacityChangeVelocity = .007;//percents per ms
        this.opacity_ += this.graph.enabled ?
            opacityChangeVelocity * timePastInMs :
            -opacityChangeVelocity * timePastInMs;
        this.opacity_ = Utils.clamp(this.opacity_, 0, 1);
    }

    preDraw(timePastInMs, scale, calculations) {
        this.processOpacityChange_(timePastInMs);

        if (calculations.totals) {
            let totals = calculations.totals;
            let fromPointIndex = scale.fromPointIndex;
            let toPointIndex = scale.toPointIndex;

            for (let pointIndex = fromPointIndex; pointIndex <= toPointIndex; pointIndex++) {
                let pointSum = this.graph.pointsData[pointIndex];
                let totalsSum = 0;

                if (totals[pointIndex]) {
                    totalsSum = totals[pointIndex];
                }

                totals[pointIndex] = totalsSum + pointSum * this.opacity_;
            }

            calculations.totals = totals;
        }
/*
        if (this.model_.summaryX && this.model_.summaryY) {
            let fromPointIndex = scale.fromPointIndex;
            let toPointIndex = scale.toPointIndex;

            let summaryData = [];
            let summaryIndex = 0;
            for (let pointIndex = fromPointIndex; pointIndex <= toPointIndex; pointIndex++) {
                let pointSum = this.graph.pointsData[pointIndex];
            }

            summaryData[this.graph.id] = [
                this.graph.enabled,
                this.graph.color,
                this.graph.label,
                this.graph.pointsData[summaryIndex]
            ];

            this.model_.summaryData = summaryData;
        } else {
            this.model_.summaryData = null;
        }*/
    }

    draw(timePastInMs, scale, opt_stack, opt_totals) {
        if (this.opacity_ !== 0) {
            this.onDraw_(timePastInMs, scale, opt_stack, opt_totals);
        }

        this.saveDrawingContext_(scale);
    }

    onDraw_(timePastInMs, scale, opt_stack, opt_totals) {}

    calculateMinMaxPointValue(scale, calculations) {
        let fromPointIndex = scale.fromPointIndex;
        let toPointIndex = scale.toPointIndex;
        let pointMaxSum = 0;
        let pointMinSum = null;
        let maxStack = calculations.maxStack;

        for (let pointIndex = fromPointIndex; pointIndex <= toPointIndex; pointIndex++) {
            let pointSum = this.graph.pointsData[pointIndex];

            if (maxStack) {
                if (!maxStack[pointIndex]) {
                    maxStack[pointIndex] = 0;
                }

                pointSum = maxStack[pointIndex] += pointSum;
            }

            if (pointSum > pointMaxSum) {
                pointMaxSum = pointSum;
            }

            if (pointSum < pointMinSum || pointMinSum === null) {
                pointMinSum = pointSum;
            }
        }

        if (pointMinSum === null) {
            pointMinSum = 0;
        }

        calculations.setMinMaxPointValues(this.graph,maxStack ? 0 : pointMinSum, pointMaxSum);
    }
}
