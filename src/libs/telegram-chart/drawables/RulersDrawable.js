import {HorizontalRuler} from "./rulers/HorizontalRuler";
import {Utils} from "../Utils";
import {ChartType} from "../basic/Chart";
import {ScaleProperties} from "../basic/ScaleProperties";

export class RulersDrawable {
    constructor(model) {
        this.model_ = model;
        this.verticalRulersTransitionTimeMs_ = 200;
        this.rulersXOffset_ = 10;

        this.rulersColorPrefix_ = '';
        this.labelsColorPrefix_ = '';
        this.verticalRulersPaintCache_ = [];

        this.horizontalRulers_ = [
            new HorizontalRuler(this.model_,6, true, false, this.labelsColorPrefix_),
            new HorizontalRuler(this.model_,6, false, true, this.labelsColorPrefix_),
            new HorizontalRuler(this.model_,6, true, true, this.labelsColorPrefix_),
        ];

        this.rulersLabelYOffset_ = 8;
        this.pxToRem_ = parseFloat(getComputedStyle(document.documentElement).fontSize);
        this.changeRulersColor(0);
    }

    drawHorizontal(timePastInMs, chart, maxValues, minValues, maxValue, minValue, scales) {
        if (chart.type === ChartType.LINE2Y) {
            this.horizontalRulers_[0].labelsColor = Utils.hexToRgbPrefix(chart.graphs[0].color);
            this.horizontalRulers_[0].lineIncluded = false;
            this.horizontalRulers_[1].labelsColor = Utils.hexToRgbPrefix(chart.graphs[1].color);

            if (!this.tmpLineScale_) {
                this.tmpLineScale_ = new ScaleProperties(this.model_);
                this.tmpLineScale_.calculateScaleY(0, 100);
                this.horizontalRulers_[2].calculateHorizontalRulerValues(100, 0, this.tmpLineScale_);
            }

            this.paintHorizontalLines_(
                this.horizontalRulers_[2].values,
                this.tmpLineScale_.unitY,
                this.tmpLineScale_.offsetY,
                1
            );
        } else {
            this.horizontalRulers_[0].labelsColor = this.labelsColorPrefix_;
            this.horizontalRulers_[1].labelsColor = this.labelsColorPrefix_
        }

        if (chart.type === ChartType.AREA) {
            this.horizontalRulers_[0].amount = 5;
        }

        /** Horizontal Rulers **/
        for (let i = 0; i < Math.min(this.horizontalRulers_.length, scales.length); i++) {
            let horizontalRuler = this.horizontalRulers_[i];
            let scale = scales[i];
            let max = scales.length > 1 ? maxValues[i] : maxValue;
            let min = scales.length > 1 ? minValues[i] : minValue;

            horizontalRuler.calculateHorizontalRulerValues(max, min, scale);

            this.paintHorizontalLabels_(
                horizontalRuler.labelValues,
                horizontalRuler.values,
                scale.unitY,
                scale.offsetY,
                horizontalRuler.progress,
                horizontalRuler.labelsRightSide,
                horizontalRuler.labelsColor
            );

            this.paintHorizontalLabels_(
                horizontalRuler.labelValuesPrev,
                horizontalRuler.valuesPrev,
                scale.unitY,
                scale.offsetY,
                1 - horizontalRuler.progress,
                horizontalRuler.labelsRightSide,
                horizontalRuler.labelsColor
            );

            if (horizontalRuler.lineIncluded) {
                this.paintHorizontalLines_(
                    horizontalRuler.values,
                    scale.unitY,
                    scale.offsetY,
                    horizontalRuler.progress
                );

                this.paintHorizontalLines_(
                    horizontalRuler.valuesPrev,
                    scale.unitY,
                    scale.offsetY,
                    1 - horizontalRuler.progress
                );
            }
        }
    }

    drawVertical(timePastInMs, scales, chart){
        this.drawVerticalRulers_(timePastInMs, scales[0], chart);
    }

    /** Vertical Rulers **/

    countVerticalRulerBinaryIterations_(leftPointIndex, rightPointIndex, minPointsInterval, iteration) {
        let delta = (rightPointIndex - leftPointIndex) / 2;

        if (minPointsInterval > delta){
            return iteration;
        }

        let middleIndex = leftPointIndex + delta;

        return this.countVerticalRulerBinaryIterations_(middleIndex, rightPointIndex, minPointsInterval, iteration + 1);
    }

    drawVerticalRulerBinary_(chart, scale, lastPointIndex, leftPointIndex, rightPointIndex, iteration, maxIteration, opacity, opt_drawLastIteration) {
        let delta = (rightPointIndex - leftPointIndex) / 2;

        if (iteration >= maxIteration){
            return;
        }

        let middleIndex = leftPointIndex + delta;

        if (middleIndex > lastPointIndex || middleIndex < 0) {
            return;
        }

        if (
            !opt_drawLastIteration ||
            (opt_drawLastIteration && iteration === maxIteration - 1)
        ) {
            let barOffset = 0;
            if (chart.type === ChartType.BAR) {
                barOffset = middleIndex / lastPointIndex
            }
            let x = scale.offsetX + (middleIndex + barOffset) * scale.unitX;

            this.paintVerticalLabel_(chart, x, opacity, middleIndex);
        }

        this.drawVerticalRulerBinary_(chart, scale, lastPointIndex, middleIndex, rightPointIndex, iteration + 1, maxIteration, opacity, opt_drawLastIteration);
        this.drawVerticalRulerBinary_(chart, scale, lastPointIndex, leftPointIndex, middleIndex, iteration + 1, maxIteration, opacity, opt_drawLastIteration);
    };

    drawVerticalRulers_(timePastInMs, scale, chart) {
        let fromPointIndex = 0;
        let lastPointIndex = chart.lastPointIndex;

        let minDistBetweenPoints = 90;
        let estimatedDist = (lastPointIndex - fromPointIndex) * scale.unitX;
        let estimatedDistBetweenPoints = estimatedDist / (lastPointIndex - fromPointIndex);

        let rulerInterval = 1;
        if (estimatedDistBetweenPoints < minDistBetweenPoints) {
            rulerInterval = Math.ceil(minDistBetweenPoints / estimatedDistBetweenPoints);
        }

        let iterations = this.countVerticalRulerBinaryIterations_(-lastPointIndex * 2, lastPointIndex * 2, rulerInterval, 0);

        if (this.prevRulerIterations_ === undefined) {
            this.prevRulerIterations_ = iterations;
            this.lastRulerIterations_ = iterations;
            this.sinceRulerIterationsChangeTime_ = 99999;
        }
        if (iterations !== this.prevRulerIterations_) {
            this.lastRulerIterations_ = this.prevRulerIterations_;
            this.sinceRulerIterationsChangeTime_ = 0;
        } else {
            this.sinceRulerIterationsChangeTime_ += timePastInMs;
        }
        this.prevRulerIterations_ = iterations;

        if (
            this.sinceRulerIterationsChangeTime_ &&
            this.sinceRulerIterationsChangeTime_ < this.verticalRulersTransitionTimeMs_
        ) {
            this.model_.triggerNextFrameRedraw = true;
        }

        let progress = Utils.clamp((this.sinceRulerIterationsChangeTime_ / this.verticalRulersTransitionTimeMs_),0, 1);
        let opacity = progress;//(progress * 2 - 1).clamp(0, 1);
        let opacityPrev = 1 - progress;//(1 - (progress * 2).clamp(0, 1));

        let labelsAdded = iterations - 1 === this.lastRulerIterations_;
        let labelsDeleted = iterations + 1 === this.lastRulerIterations_;

        if (labelsDeleted) {
            opacity = 1;
        }

        if (labelsAdded) {
            opacityPrev = 1;
        }

        this.drawVerticalRulerBinary_(chart, scale, lastPointIndex,-lastPointIndex * 2, lastPointIndex * 2, 0, iterations, opacity, labelsAdded);

        if (opacityPrev > 0) {
            this.drawVerticalRulerBinary_(chart, scale, lastPointIndex,-lastPointIndex * 2, lastPointIndex * 2, 0, this.lastRulerIterations_, opacityPrev, labelsDeleted);
        }
    }

    /** Paint Methods **/

    paintHorizontalLines_(lineValues, unitY, offsetY, opacity) {
        if (opacity === 0) {
            return;
        }

        let context = this.model_.canvasContext;

        context.lineWidth = '1';//'.8';
        context.strokeStyle = this.rulersColorPrefix_ + (opacity * 0.7) + ')';

        for (let lineId = 0; lineId < lineValues.length; lineId++) {
            let lineValue = lineValues[lineId];

            let lineYPx = this.model_.canvasHeight - (offsetY + lineValue * unitY);

            if (
                lineYPx - 1 > this.model_.canvasHeight + this.model_.canvasPaddingTop + this.model_.canvasPaddingBottom ||
                lineYPx + 1 < -this.model_.canvasPaddingTop
            ) {
                continue;
            }

            context.beginPath();
            context.moveTo(this.rulersXOffset_, lineYPx);
            context.lineTo(this.model_.canvasWidth - this.rulersXOffset_, lineYPx);
            context.stroke();
            context.closePath();
        }
    }

    paintHorizontalLabels_(labelValues, lineValues, unitY, offsetY, opacity, right, colorPrefix) {
        if (opacity === 0) {
            return;
        }

        let context = this.model_.canvasContext;

        context.font = '700 ' + (this.pxToRem_ * 0.75) + 'px Roboto';
        context.fillStyle =  colorPrefix + opacity + ')';

        for (let lineId = 0; lineId < lineValues.length; lineId++) {
            let labelValue = labelValues[lineId];
            let lineValue = lineValues[lineId];

            let lineYPx = this.model_.canvasHeight - (offsetY + lineValue * unitY) - this.rulersLabelYOffset_;
            let label =  Utils.parsePointValueShorten(labelValue).toString();

            let lineXPx = this.rulersXOffset_;

            if (right) {
                let labelWidth = context.measureText(label).width;
                lineXPx = this.model_.canvasWidth - labelWidth - this.rulersXOffset_;
            }

            if (
                lineYPx - this.pxToRem_ - 1 > this.model_.canvasHeight + this.model_.canvasPaddingTop + this.model_.canvasPaddingBottom ||
                lineYPx + 1 < -this.model_.canvasPaddingTop
            ) {
                continue;
            }

            context.fillText(label, lineXPx, lineYPx);
        }
    }

    paintVerticalLabel_(chart, lineXPx, opacity, pointIndex) {
        let context = this.model_.canvasContext;
        let timestamp = chart.timelineStart + chart.timelineStep * pointIndex;

        let labelWidth;
        let label;
        if (this.verticalRulersPaintCache_[timestamp]) {
            label = this.verticalRulersPaintCache_[timestamp][0];
            labelWidth = this.verticalRulersPaintCache_[timestamp][1];
        } else {
            label = Utils.parsePointIndex(chart.timelineStart, chart.timelineStep, pointIndex).toString();
            labelWidth = context.measureText(label).width + 1;
            this.verticalRulersPaintCache_[timestamp] = [label, labelWidth];
        }

        if (
            lineXPx + labelWidth + 1 < 0 ||
            lineXPx - labelWidth - 1 > this.model_.canvasWidth
        ) {
            return;
        }

        let offsetPercent = lineXPx / this.model_.canvasWidth;

        context.font = '700 ' + (this.pxToRem_ * 0.75) + 'px Roboto';
        context.fillStyle = this.labelsColorPrefix_ + opacity + ')';
        context.fillText(label, lineXPx - labelWidth * offsetPercent, this.model_.canvasHeight + this.model_.canvasPaddingBottom - this.rulersLabelYOffset_);
    }

    changeRulersColor(progress) {
        let rulersDayColor = [200, 200, 200];
        let rulersNightColor = [61, 63, 78];

        let labelsDayColor = [21, 21, 21];
        let labelsNightColor = [84, 113, 130];

        this.rulersColorPrefix_ = 'rgba('
            + (Math.round(rulersDayColor[0] * (1 - progress) + rulersNightColor[0] * progress)) + ',' +
            + (Math.round(rulersDayColor[1] * (1 - progress) + rulersNightColor[1] * progress)) + ',' +
            + (Math.round(rulersDayColor[2] * (1 - progress) + rulersNightColor[2] * progress)) + ',';

        this.labelsColorPrefix_ = 'rgba('
            + (Math.round(labelsDayColor[0] * (1 - progress) + labelsNightColor[0] * progress)) + ',' +
            + (Math.round(labelsDayColor[1] * (1 - progress) + labelsNightColor[1] * progress)) + ',' +
            + (Math.round(labelsDayColor[2] * (1 - progress) + labelsNightColor[2] * progress)) + ',';
    }
}
