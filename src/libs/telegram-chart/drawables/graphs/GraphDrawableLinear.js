import {GraphDrawable} from "../GraphDrawable";

export class GraphDrawableLinear extends GraphDrawable {
    constructor(model, graph) {
        super(model, graph);
    }

    onDraw_(timePastInMs, scale, calculations) {
        let canvasContext = this.model_.canvasContext;
        let fromPointIndex = scale.fromPointIndex;
        let toPointIndex = scale.toPointIndex;
        let pointMaxSum = 0;
        let pointMinSum = null;

        canvasContext.beginPath();
        canvasContext.lineJoin = 'round';
        canvasContext.lineCap = 'round';
        canvasContext.strokeStyle = this.rgbPrefix_ + this.opacity_ + ')';
        canvasContext.lineWidth = '2';

        for (let pointIndex = fromPointIndex; pointIndex <= toPointIndex; pointIndex++) {
            let pointSum = this.graph.pointsData[pointIndex];

            if (pointSum > pointMaxSum) {
                pointMaxSum = pointSum;
            }

            if (pointSum < pointMinSum || pointMinSum === null) {
                pointMinSum = pointSum;
            }

            let y = this.model_.canvasHeight - (scale.offsetY + pointSum * scale.unitY);
            let x = scale.offsetX + pointIndex * scale.unitX;

            if (pointIndex === fromPointIndex) {
                canvasContext.moveTo(x, y);
            }

            canvasContext.lineTo(x, y);
        }

        if (pointMinSum === null) {
            pointMinSum = 0;
        }

        canvasContext.stroke();
        canvasContext.closePath();

        calculations.setMinMaxPointValues(this.graph, pointMinSum, pointMaxSum);
    }
}
