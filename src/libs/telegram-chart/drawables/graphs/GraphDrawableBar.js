import {GraphDrawable} from "../GraphDrawable";
import {Utils} from "../../Utils";

export class GraphDrawableBar extends GraphDrawable {
    constructor(model, graph) {
        super(model, graph);
        this.leftOffsetX_ = 0;
        this.opacityChangeDelay_ = 200;
        this.opacityChangeDelayProgress_ = 0;
    }

    processOpacityChange_(timePastInMs){
        if (!this.graph.enabled) {
            this.opacityChangeDelayProgress_ = this.opacityChangeDelay_;
        }
        if (this.graph.enabled && this.opacityChangeDelayProgress_ > 0) {
            this.opacityChangeDelayProgress_ -= timePastInMs;
            this.opacity_ = 0.0001;
        } else {
            let opacityChangeVelocity = .005;//percents per ms
            this.opacity_ += this.graph.enabled ?
                opacityChangeVelocity * timePastInMs :
                -opacityChangeVelocity * timePastInMs;
            this.opacity_ = Utils.clamp(this.opacity_, 0, 1);
        }
    }

    onDraw_(timePastInMs, scale, calculations) {
        let canvasContext = this.model_.canvasContext;
        let fromPointIndex = scale.fromPointIndex;
        let toPointIndex = scale.toPointIndex;
        let pointMaxSum = 0;
        let stack = calculations.stack;
        let maxStack = calculations.maxStack;

        canvasContext.beginPath();
        canvasContext.lineJoin = 'miter';
        canvasContext.lineCap = 'butt';
        const isHighlighted = this.model_.summaryPointIndex !== null;
        let defaultFillStyle = this.rgbPrefix_ + (!isHighlighted ? '1)' : '.6)');
        let highlightFillStyle = this.rgbPrefix_ + '1)';
        canvasContext.lineWidth = '0';

        this.leftOffsetX_ = scale.offsetX + fromPointIndex * scale.unitX;
        for (let pointIndex = fromPointIndex; pointIndex <= toPointIndex; pointIndex++) {
            let pointSum = this.graph.pointsData[pointIndex];
            let stackSum = stack[pointIndex] ? stack[pointIndex] : 0;
            let maxStackSum = maxStack[pointIndex] ? maxStack[pointIndex] : 0;

            if (pointSum + maxStackSum > pointMaxSum) {
                pointMaxSum = pointSum + maxStackSum;
            }

            if (pointIndex === this.model_.summaryPointIndex) {
                canvasContext.fillStyle = highlightFillStyle;
            } else {
                canvasContext.fillStyle = defaultFillStyle;
            }

            let y = this.model_.canvasHeight - (scale.offsetY + (pointSum * this.opacity_ + stackSum) * scale.unitY);
            let width = scale.unitX;
            let height = pointSum * this.opacity_ * scale.unitY;

            canvasContext.fillRect(Math.floor(this.leftOffsetX_), y, Math.floor(this.leftOffsetX_ + width) - Math.floor(this.leftOffsetX_) + 1, height);
            this.leftOffsetX_ += width;

            stack[pointIndex] = stackSum + pointSum * this.opacity_;
            maxStack[pointIndex] = maxStackSum + ((this.opacity_ !== 0) ? pointSum : 0);
        }

        calculations.setMinMaxPointValues(this.graph, 0, pointMaxSum);
    }
}
