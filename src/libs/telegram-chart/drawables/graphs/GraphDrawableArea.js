import {GraphDrawable} from "../GraphDrawable";
import {Utils} from "../../Utils";

export class GraphDrawableArea extends GraphDrawable {
    constructor(model, graph) {
        super(model, graph);
    }

    processOpacityChange_(timePastInMs){
        let opacityChangeVelocity = .006;//percents per ms
        this.opacity_ += this.graph.enabled ?
            opacityChangeVelocity * timePastInMs :
            -opacityChangeVelocity * timePastInMs;
        this.opacity_ = Utils.clamp(this.opacity_, 0, 1);
    }

    onDraw_(timePastInMs, scale, calculations) {
        let canvasContext = this.model_.canvasContext;
        let fromPointIndex = scale.fromPointIndex;
        let toPointIndex = scale.toPointIndex;
        let stack = calculations.stack;
        let totals = calculations.totals;

        let unitY = this.model_.canvasHeight;

        canvasContext.beginPath();
        canvasContext.lineJoin = 'miter';
        canvasContext.lineCap = 'butt';
        canvasContext.fillStyle = this.rgbPrefix_ + '1)';
        canvasContext.lineWidth = '0';

        for (let pointIndex = fromPointIndex; pointIndex <= toPointIndex; pointIndex++) {
            let pointSum = this.graph.pointsData[pointIndex] / totals[pointIndex];
            let stackSum = 0;

            if (stack[pointIndex]) {
                stackSum = stack[pointIndex];
            }

            let y = this.model_.canvasHeight - ((pointSum * this.opacity_ + stackSum) * unitY);
            let x = scale.offsetX + pointIndex * scale.unitX;

            if (pointIndex === fromPointIndex) {
                canvasContext.moveTo(x, this.model_.canvasHeight  - (stackSum * unitY));
            }

            canvasContext.lineTo(x, y);

            if (pointIndex === toPointIndex) {
                canvasContext.lineTo(x, this.model_.canvasHeight  - (stackSum * unitY));
            }

            stack[pointIndex] = stackSum + pointSum * this.opacity_;
        }

        for (let pointIndex = toPointIndex - 1; pointIndex >= fromPointIndex + 1; pointIndex--) {
            let pointSum = this.graph.pointsData[pointIndex] / totals[pointIndex];
            let stackSum = stack[pointIndex] - pointSum * this.opacity_;

            let y = this.model_.canvasHeight - (stackSum * unitY);
            let x = scale.offsetX + pointIndex * scale.unitX;

            canvasContext.lineTo(x, y);
        }
        canvasContext.fill();
        canvasContext.closePath();

        calculations.setMinMaxPointValues(this.graph, 0, 100);
    }

    calculateMinMaxPointValue(scale, calculations) {
        calculations.setMinMaxPointValues(this.graph, 0, 100);
    }
}
