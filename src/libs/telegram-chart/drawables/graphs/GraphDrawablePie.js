import {GraphDrawable} from "../GraphDrawable";

export class GraphDrawablePie extends GraphDrawable {
    constructor(model, graph) {
        super(model, graph);
    }

    processOpacityChange_(timePastInMs){
        let opacityChangeVelocity = .004;//percents per ms
        this.opacity_ += this.graph.enabled ?
            opacityChangeVelocity * timePastInMs :
            -opacityChangeVelocity * timePastInMs;
        this.opacity_ = this.opacity_.clamp(0, 1);
    }

    onDraw_(timePastInMs, scale, calculations) {
        let canvasContext = this.model_.canvasContext;
        let fromPointIndex = scale.fromPointIndex;
        let toPointIndex = scale.toPointIndex;
        let stack = calculations.stack;
        let totals = calculations.totals;

        canvasContext.beginPath();
        canvasContext.lineJoin = 'miter';
        canvasContext.lineCap = 'butt';
        canvasContext.fillStyle = this.rgbPrefix_ + '1)';
        canvasContext.lineWidth = '0';

        let sectorPointSum = 0;
        let sectorTotalsSum = 0;

        for (let pointIndex = fromPointIndex; pointIndex <= toPointIndex; pointIndex++) {
            let pointSum = this.graph.pointsData[pointIndex];
            let totalsSum = totals[pointIndex];

            sectorPointSum += pointSum * this.opacity_;
            sectorTotalsSum += totalsSum;
        }

        let sectorRad = sectorPointSum / sectorTotalsSum  * 2 * Math.PI;
        let startRad = Math.PI / 2;
        if (stack.length) {
            startRad = stack[0];
        } else {
            stack[0] = startRad;
        }

        let highlighted = false;
        if (
            this.model_.summaryX !== null &&
            this.model_.summaryY !== null
        ) {
            let radius = this.model_.canvasHeight / 2;
            let x = this.model_.canvasWidth / 2;
            let y = this.model_.canvasHeight / 2 + this.model_.canvasPaddingTop;
            let angle = Math.atan2(this.model_.summaryY - y, this.model_.summaryX - x);

            if (
                Math.sqrt(Math.pow(x - this.model_.summaryX, 2) + Math.pow(y - this.model_.summaryY, 2)) <= radius &&
                (
                    startRad <= angle && angle < startRad + sectorRad ||
                    startRad <= angle + Math.PI * 2 && angle + Math.PI * 2 < startRad + sectorRad
                )
            ) {
                highlighted = true;
            }
        }

        if (highlighted) {
            this.model_.summaryGraphId = this.graph.id;
            this.model_.summaryGraphSum = sectorPointSum;
        }

        let centerRad = startRad + sectorRad / 2;
        let centerDelta = highlighted ? 10 : 0;
        let centerDeltaX = centerDelta * Math.cos(centerRad);
        let centerDeltaY = centerDelta * Math.sin(centerRad);

        let radius = this.model_.canvasHeight / 2;
        let x = this.model_.canvasWidth / 2 + centerDeltaX;
        let y = this.model_.canvasHeight / 2 + centerDeltaY;

        let labelX = x + Math.cos(centerRad) * (radius / 3 * 2.2);
        let labelY = y + Math.sin(centerRad) * (radius / 3 * 2.2);

        canvasContext.lineTo(x, y);
        canvasContext.arc(x, y, radius, startRad, startRad + sectorRad);
        canvasContext.moveTo(x, y);

        canvasContext.fill();
        canvasContext.closePath();

        canvasContext.fillStyle = '#fff';
        let percent = Math.round( sectorPointSum / sectorTotalsSum * 100);
        let labelSize = 6 + 18 * (percent / 6).clamp(0, 1);
        canvasContext.font = '500 '+labelSize+'px Arial';
        let label = percent + '%';
        let labelMeasures = canvasContext.measureText(label);
        canvasContext.fillText(label, labelX - labelMeasures.width / 2, labelY + labelSize / 2);

        stack[0] += sectorRad;

        calculations.setMinMaxPointValues(this.graph, 0, 100);
    }

    calculateMinMaxPointValue(scale, calculations) {
        calculations.setMinMaxPointValues(this.graph, 0, 100);
    }
}
