import {ChartType} from "../basic/Chart";
import {GraphDrawableLinear} from "./graphs/GraphDrawableLinear";
import {GraphDrawableBar} from "./graphs/GraphDrawableBar";
import {GraphDrawableArea} from "./graphs/GraphDrawableArea";
import {GraphDrawablePie} from "./graphs/GraphDrawablePie";
import {DrawingCalculations} from "../basic/DrawingCalculations";
import {ScaleProperties} from "../basic/ScaleProperties";

export class ChartDrawable {
    constructor(model, opt_animateFactor) {
        this.model_ = model;
        this.graphDrawables_ = [];

        let graphs = model.chart.graphs;
        for (let graphIndex in graphs) {
            let graphDrawable = null;
            switch (model.chart.type) {
                case ChartType.LINE :
                case ChartType.LINE2Y :
                    graphDrawable = new GraphDrawableLinear(model, graphs[graphIndex]);
                    break;
                case ChartType.BAR :
                    graphDrawable = new GraphDrawableBar(model, graphs[graphIndex]);
                    break;
                case ChartType.AREA :
                    graphDrawable = new GraphDrawableArea(model, graphs[graphIndex]);
                    break;
                case ChartType.PIE :
                    graphDrawable = new GraphDrawablePie(model, graphs[graphIndex]);
                    break;
            }
            this.graphDrawables_.push(graphDrawable);
        }

        switch (model.chart.type) {
            case ChartType.BAR :
                this.calculations = new DrawingCalculations(true);
                break;
            case ChartType.AREA :
            case ChartType.PIE :
                this.calculations = new DrawingCalculations(true, true);
                this.calculations = new DrawingCalculations(true, true);
                break;
            default :
                this.calculations = new DrawingCalculations();
        }

        this.scales = [];
        if (model.chart.type === ChartType.LINE2Y) {
            for (let i = 0; i < model.chart.graphs.length; i++) {
                this.scales[i] = new ScaleProperties(model);
            }
        } else {
            this.scales[0] = new ScaleProperties(model);
        }

        this.calculateScaleX_(model.rangeFrom, model.rangeTo);
        this.calculateMinMaxPointValues_();

        if (opt_animateFactor) {
            let factor = opt_animateFactor;

            this.calculations.maxPointValue *= factor;
            this.calculations.minPointValue /= factor;
            for (let i = 0; i < this.calculations.maxPointValues.length; i++) {
                this.calculations.maxPointValues[i] *= factor;
            }
            for (let i = 0; i < this.calculations.minPointValues.length; i++) {
                this.calculations.minPointValues[i] /= factor;
            }
        }
        this.calculateScaleY_(0, true);
    }

    isNeedsToBeRedrawn() {
        for (let graphIndex = 0; graphIndex < this.graphDrawables_.length; graphIndex++) {
            let graphDrawable = this.graphDrawables_[graphIndex];

            if (graphDrawable.isNeedsToBeRedrawn(this.scales[Math.min(graphIndex, this.scales.length - 1)])) {
                return true;
            }
        }

        return false;
    }

    preDraw(timePastInMs) {
        this.calculations.reset();

        this.calculateScaleX_(this.model_.rangeFrom, this.model_.rangeTo);

        for (let graphIndex = 0; graphIndex < this.graphDrawables_.length; graphIndex++) {
            let graphDrawable = this.graphDrawables_[graphIndex];
            let scaleIndex = Math.min(graphIndex, this.scales.length - 1);

            graphDrawable.preDraw(timePastInMs, this.scales[scaleIndex], this.calculations);
        }
    }

    draw(timePastInMs){
        this.render_(timePastInMs);
    }

    postDraw(timePastInMs) {
        this.calculateScaleY_(timePastInMs);
    }

    calculateMinMaxPointValues_() {
        this.calculations.reset();
        this.render_();
    }

    calculateScaleX_(fromPercent, toPercent) {
        for (let i = 0; i < this.scales.length; i++) {
            this.scales[i].calculateScaleX(fromPercent, toPercent);
            this.scales[i].calculateBounds();
        }
    }

    calculateScaleY_(timePastInMs, opt_instantChange) {
        if (this.scales.length > 1) {
            for (let scaleIndex = 0; scaleIndex < this.scales.length; scaleIndex++) {
                this.scales[scaleIndex].calculateScaleY(
                    this.calculations.minPointValues[scaleIndex],
                    this.calculations.maxPointValues[scaleIndex],
                    timePastInMs,
                    opt_instantChange
                );
            }
        } else {
            this.scales[0].calculateScaleY(this.calculations.minPointValue, this.calculations.maxPointValue, timePastInMs, opt_instantChange);
        }
    }

    render_(timePastInMs) {
        let isDrawing = timePastInMs !== undefined && timePastInMs !== null;

        for (let graphIndex = 0; graphIndex < this.graphDrawables_.length; graphIndex++) {
            let graphDrawable = this.graphDrawables_[graphIndex];
            let scaleIndex = Math.min(graphIndex, this.scales.length - 1);

            if (isDrawing) {
                graphDrawable.draw(timePastInMs, this.scales[scaleIndex], this.calculations);
            } else {
                graphDrawable.calculateMinMaxPointValue(this.scales[scaleIndex], this.calculations)
            }
        }
    }
}
