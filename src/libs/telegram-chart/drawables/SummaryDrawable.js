import {ChartType} from "../basic/Chart";

export class SummaryDrawable {
    constructor(model, canvasElement, summaryElement, summaryElementLine) {
        this.model_ = model;

        this.canvasElement_ = canvasElement;

        this.summaryElement_ = summaryElement;
        this.summaryElementLine_ = summaryElementLine;
        this.summaryElementTimelene_ = summaryElement.querySelector('.charts-summary-timeline');
        this.summaryElementGraphs_ = summaryElement.querySelector('.charts-summary-graphs');
        this.summaryElementGraphPattern_ = summaryElement.querySelector('.charts-summary-graph');
        this.summaryElementLineDotPattern_ = summaryElementLine.querySelector('.charts-summary-line-dot');
        this.summaryElementGraphPattern_.remove();
        this.summaryElementLineDotPattern_.remove();

        window.addEventListener('mousemove', this.showSummary_.bind(this));
        window.addEventListener('touchmove', this.showSummary_.bind(this));
        window.addEventListener('mouseleave', this.hideSummary_.bind(this));
        this.summaryElement_.addEventListener('mousemove', this.showSummary_.bind(this));
        window.addEventListener('touchstart', function (event) {
            if (event.target !== canvasElement) {
                this.hideSummary_();
            }
        }.bind(this));
    }

    showSummary_(event) {
        let touchX;
        let touchY;
        const rect = this.canvasElement_.getBoundingClientRect();
        const documentRect = document.body.getBoundingClientRect();

        if (event.changedTouches && event.changedTouches.length) {
            touchX = event.changedTouches[event.changedTouches.length - 1].pageX;
            touchY = event.changedTouches[event.changedTouches.length - 1].pageY + documentRect.top;
        } else {
            touchX = event.pageX;
            touchY = event.pageY + documentRect.top;
        }

        if (touchY >= rect.top && touchY <= rect.bottom) {
            this.model_.setSummary(touchX, touchY);
        } else {
            this.hideSummary_();
        }
    }

    hideSummary_() {
        this.model_.setSummary(null, null, null);
        this.summaryElement_.style.visibility = 'hidden';
        this.summaryElementLine_.style.visibility = 'hidden';
    }

    findOutPointIndex(scales) {
        if (
            this.model_.summaryX === null ||
            this.model_.summaryY === null
        ) {
            return;
        }

        let chart = this.model_.chart;
        let halphPluss = chart.type === ChartType.BAR ? -0.5 : 0;

        let offsetX = scales[0].offsetX;
        let unitX = scales[0].unitX;

        let pointIndex = Math.round((this.model_.summaryX - offsetX) / unitX + halphPluss);
        pointIndex = pointIndex.clamp(0, chart.lastPointIndex);
        if (this.model_.summaryPointIndex !== pointIndex) {
            this.model_.setSummary(this.model_.summaryX, this.model_.summaryY, pointIndex);
        }

        return pointIndex;
    }

    draw(scales) {
        if (
            this.model_.summaryX === null ||
            this.model_.summaryY === null
        ) {
            return;
        }
        let chart = this.model_.chart;

        let includeLine = !(
            chart.type === ChartType.BAR ||
            chart.type === ChartType.PIE
        );

        let includeDots = !(
            chart.type === ChartType.BAR ||
            chart.type === ChartType.AREA ||
            chart.type === ChartType.PIE
        );

        let includePercents = (
            chart.type === ChartType.AREA
        );

        let includeTotal = (
            chart.type === ChartType.BAR
        );

        let totalSum = 0;
        let enabedGraphsAmount = 0;

        let offsetX = scales[0].offsetX;
        let unitX = scales[0].unitX;

        let pointIndex = this.findOutPointIndex(scales);
        let lineX = offsetX + pointIndex * unitX;

        let timelineLabel = Utils.parsePointIndex(chart.timelineStart, chart.timelineStep, pointIndex, true);
        this.summaryElement_.setAttribute('data-time', chart.timelineStart + pointIndex * chart.timelineStep);

        this.summaryElementLine_.innerHTML = '';
        this.summaryElementGraphs_.innerHTML = '';

        if (
            this.model_.chart.type === ChartType.PIE
        ) {
            this.summaryElementTimelene_.style.display = 'none';
        } else {
            this.summaryElementTimelene_.style.display = 'block';
            this.summaryElementTimelene_.innerHTML = timelineLabel;
        }

        if (includeTotal || includePercents) {
            for (let graphIndex = 0; graphIndex < chart.graphs.length; graphIndex++) {
                let graph = chart.graphs[graphIndex];
                if (graph.enabled) {
                    totalSum += graph.pointsData[pointIndex];
                    enabedGraphsAmount++;
                }
            }
        }

        for (let graphIndex = 0; graphIndex < chart.graphs.length; graphIndex++) {
            let graph = chart.graphs[graphIndex];

            if (graph.enabled && (
                (graphIndex === this.model_.summaryGraphId && this.model_.chart.type === ChartType.PIE) ||
                this.model_.chart.type !== ChartType.PIE
            )) {
                let pointSum = graph.pointsData[pointIndex];

                if (
                    this.model_.chart.type === ChartType.PIE &&
                    this.model_.summaryGraphSum !== null
                ) {
                    pointSum = this.model_.summaryGraphSum;
                }

                let scaleIndex = Math.min(scales.length - 1, graphIndex);
                let scale = scales[scaleIndex];

                let y = this.model_.canvasHeight - pointSum * scale.unitY - scale.offsetY;

                let graphElement = this.summaryElementGraphPattern_.cloneNode(true);
                let graphElementValue = graphElement.querySelector('.charts-summary-value');
                graphElementValue.style.color = graph.color;
                graphElementValue.innerHTML = Utils.parsePointValue(pointSum);
                let labelHtml = graph.label;
                if (includePercents) {
                    labelHtml = '<strong>' + Math.round(pointSum / totalSum * 100) + '%</strong> ' + labelHtml;
                }

                graphElement.querySelector('.charts-summary-label').innerHTML = labelHtml;
                this.summaryElementGraphs_.appendChild(graphElement);

                if (includeDots) {
                    let dotElement = this.summaryElementLineDotPattern_.cloneNode();
                    dotElement.style.top = y + 'px';
                    dotElement.style.borderColor = graph.color;
                    this.summaryElementLine_.appendChild(dotElement);
                }
            }
        }

        if (includeTotal && enabedGraphsAmount > 1) {
            let graphElement = this.summaryElementGraphPattern_.cloneNode(true);
            let graphElementValue = graphElement.querySelector('.charts-summary-value');
            graphElementValue.innerHTML = Utils.parsePointValue(totalSum);
            graphElement.querySelector('.charts-summary-label').innerHTML = 'All';
            this.summaryElementGraphs_.appendChild(graphElement);
        }

        if (includeLine) {
            this.summaryElementLine_.style.visibility = 'visible';
        } else {
            this.summaryElementLine_.style.visibility = 'hidden';
        }

        if (
            this.model_.chart.type === ChartType.PIE
        ) {
            if (this.model_.summaryGraphSum !== null) {
                this.summaryElement_.style.visibility = 'visible';
            } else {
                this.summaryElement_.style.visibility = 'hidden';
            }

            let x = this.model_.summaryX;
            let y = this.model_.summaryY;
            this.summaryElement_.style.transform = 'translateX(calc('+ x + 'px - '+Math.round(x / this.model_.canvasWidth * 100)+'%))' +
                'translateY(calc('+ y + 'px - '+Math.round(y / this.model_.canvasWidth * 100)+'%))';
        } else {
            if (
                this.model_.chart.type === ChartType.BAR
            ) {
                let transformRight = lineX / this.model_.canvasWidth <= .5;
                this.summaryElement_.style.transform = 'translateX(calc('+ lineX + 'px '+(transformRight ? ' + '+Math.round(unitX)+'px + 10px' : ' - 100% - 10px')+'))';
            } else {
                this.summaryElement_.style.transform = 'translateX(calc('+ lineX + 'px - '+Math.round(lineX / this.model_.canvasWidth * 100)+'%))';
                this.summaryElementLine_.style.transform = 'translateX(calc('+ lineX + 'px - '+Math.round(lineX / this.model_.canvasWidth * 100)+'%))';
            }
            this.summaryElement_.style.visibility = 'visible';
        }
    }
}
