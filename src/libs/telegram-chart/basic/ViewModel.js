import {Utils} from "../Utils";

export class ViewModel {
    constructor(canvasElement) {
        this.canvasElement_ = canvasElement;
        this.canvasContext = canvasElement.getContext('2d');
        this.canvasWidth = 0;
        this.canvasHeight = 0;
        this.canvasPaddingTop = 0;
        this.canvasPaddingBottom = 0;
        this.triggerNextFrameRedraw = false;
        this.summaryX = null;
        this.summaryY = null;
        this.summaryPointIndex = null;

        this.calculateCanvasBounds();
    }

    setRange(rangeFrom, rangeTo) {
        this.rangeFrom = rangeFrom;
        this.rangeTo = rangeTo;
        this.triggerNextFrameRedraw = true;
    }

    setSummary(summaryX, summaryY, opt_pointIndex) {
        this.summaryX = summaryX;
        this.summaryY = summaryY;
        this.summaryGraphId = null;
        this.summaryGraphSum = null;
        this.summaryPointIndex = (opt_pointIndex !== undefined && opt_pointIndex !== null) ? opt_pointIndex : null;
        this.triggerNextFrameRedraw = true;
    }

    setChart(chart) {
        this.chart = chart;
        this.triggerNextFrameRedraw = true;
    }

    calculateCanvasBounds() {
        const width = this.canvasElement_.offsetWidth;
        const height = this.canvasElement_.offsetHeight;
        this.canvasWidth = this.canvasElement_.width = width;
        this.canvasHeight = (this.canvasElement_.height = height) - this.canvasPaddingTop - this.canvasPaddingBottom;

        let scale = Utils.clamp(window.devicePixelRatio, 1, 2);
        this.canvasElement_.height *= scale;
        this.canvasElement_.width *= scale;
        this.canvasContext.scale(scale, scale);

        this.canvasContext.translate(0, this.canvasPaddingTop);
        this.triggerNextFrameRedraw = true;
    }

/*    calculateScaleX(fromPercent, toPercent) {
        for (let i = 0; i < this.scales.length; i++) {
            this.scales[i].calculateScaleX(fromPercent, toPercent);
        }
    }

    calculateScaleY(minValue, maxValue, timePastInMs, opt_instantChange) {
        for (let i = 0; i < this.scales.length; i++) {
            this.scales[i].calculateScaleY(minValue, maxValue, timePastInMs, opt_instantChange);
        }
    }*/
}
