import {ChartType} from "./Chart";
import {Utils} from "../Utils";

export class ScaleProperties {
    constructor(model) {
        this.model_ = model;
        this.unitX = 0;
        this.unitY = 0;
        this.offsetX = 0;
        this.offsetY = 0;
    }

    calculateBounds() {
        //TODO: move to scale
        let fromPointIndex = Utils.clamp(Math.floor(-this.offsetX / this.unitX), 0, this.model_.chart.lastPointIndex);
        let toPointIndex = Utils.clamp(Math.ceil((this.model_.canvasWidth - this.offsetX) / this.unitX),0, this.model_.chart.lastPointIndex);

        if (isNaN(fromPointIndex)) {
            fromPointIndex = 0;
        }

        if (isNaN(toPointIndex)) {
            toPointIndex = this.model_.chart.lastPointIndex;
        }

        this.fromPointIndex = fromPointIndex;
        this.toPointIndex = toPointIndex;
    }

    calculateScaleX(fromPercent, toPercent) {
        let fullPointWidth = this.model_.chart.lastPointIndex;

        if (this.model_.chart.type === ChartType.BAR) {
            fullPointWidth++;
        }

        let range = (toPercent - fromPercent);
        this.unitX = this.model_.canvasWidth / fullPointWidth / range;

        let fromProgress = fromPercent * fullPointWidth * this.unitX;
        this.offsetX = -fromProgress;
    }

    calculateScaleY(minValue, maxValue, timePastInMs, opt_instantChange) {
        if (maxValue !== 0) {
            let unitYHasToBe = this.model_.canvasHeight / (maxValue - minValue);
            let offsetYHasToBe = - minValue * unitYHasToBe;

            let deltaY = ((unitYHasToBe - this.unitY) / 100 * timePastInMs);
            let deltaOffsetY = ((offsetYHasToBe - this.offsetY) / 100 * timePastInMs);

            /*if (unitYHasToBe < this.unitY) {
                deltaY = ((unitYHasToBe - this.unitY) / 100 * timePastInMs);
            }*/

            if (
                Math.abs(maxValue * deltaY) < .1 ||
                Math.sign(unitYHasToBe - this.unitY) !==
                Math.sign(unitYHasToBe - this.unitY - deltaY) ||
                opt_instantChange
            ) {
                //Движения меньше четверти пикселя скипаем
                //Восстанавливаем нужное значения, если проехали остановочку с дэльтой
                this.unitY = unitYHasToBe;
                this.offsetY = offsetYHasToBe;
            } else {
                this.unitY += deltaY;
                this.offsetY += deltaOffsetY;
            }
        }
    }
}
