export class Graph {
    constructor(id, pointsData, color, label) {
        this.id = id;
        this.pointsData = pointsData;
        this.color = color;
        this.label = label;
        this.enabled = true;
    }
}
