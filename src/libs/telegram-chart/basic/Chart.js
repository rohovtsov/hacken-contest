import {Graph} from "./Graph";

export var ChartType = {
    LINE: 'line',
    LINE2Y: 'line2y',
    BAR: 'bar',
    AREA: 'area',
    PIE: 'pie'
};

export class Chart {
    constructor(chartData) {
        this.data = chartData;
        this.graphs = [];
        this.timelineStep = 0;
        this.timelineStart = 0;
        this.pointsIndexLength = 0;
        this.type = chartData['type'];
        let graphData = chartData['graphs'];

        for (let i = 0; i < graphData.length; i++) {
            let graphColor = graphData[i]['color'];
            let graphLabel = graphData[i]['label'];
            let graphPointsData = graphData[i]['data'];
            let graph = new Graph(i, graphPointsData, graphColor, graphLabel);

            if (graphData[i]['disabled']) {
                graph.enabled = false;
            }

            this.graphs.push(graph);
            this.pointsIndexLength = graphPointsData.length > this.pointsIndexLength ? graphPointsData.length : this.pointsIndexLength;
        }
        this.lastPointIndex = this.pointsIndexLength - 1;

        this.timelineStart = parseFloat(chartData['time']);
        this.timelineStep = parseFloat(chartData['interval']);
    }
}
