export class DrawingCalculations {
    constructor(calculateStack, calculateTotals) {
        this.stack = calculateStack ? [] : null;
        this.totals = calculateTotals ? [] : null;

        this.maxStack = calculateStack ? [] : null;
        this.maxPointValues = [];
        this.minPointValues = [];
    }

    reset() {
        if (this.totals) {
            this.totals = [];
        }
        if (this.stack) {
            this.stack = [];
            this.maxStack = [];
        }
        this.maxPointValues = [];
        this.minPointValues = [];
        this.maxPointValue = 0;
        this.minPointValue = 0;
    }

    setMinMaxPointValues(graph, minPointValue, maxPointValue){
        if (!graph.enabled) {
            this.maxPointValues[graph.id] = 0;
            this.minPointValues[graph.id] = 0;
            return;
        }

        this.maxPointValues[graph.id] = maxPointValue;
        this.minPointValues[graph.id] = minPointValue;

        if (maxPointValue > this.maxPointValue) {
            this.maxPointValue = maxPointValue;
        }

        if (minPointValue < this.minPointValue || this.minPointValue === 0) {
            this.minPointValue = minPointValue;
        }
    }
}
