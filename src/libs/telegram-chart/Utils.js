export class Utils {
    static clamp(num, min, max) {
      return Math.min(Math.max(num, min), max);
    }

    static parsePointValue(pointValue) {
        if (pointValue >= 10000) {
            let parsedValue = '';
            let position = 0;

            do {
                let number = pointValue % 10;
                pointValue = Math.floor(pointValue / 10);

                parsedValue = (
                    number +
                    (position++ % 3 === 0 ? ' ' : '')
                ) + parsedValue;
            } while (pointValue > 0);

            return parsedValue;
        } else {
            return pointValue;
        }
    }

    static parsePointValueShorten(pointValue) {
        if (pointValue < 10000) {
            return pointValue.toString();
        } else {
            pointValue = Math.round(pointValue);
            let stringValue = pointValue.toString();

            switch (stringValue.length) {
                case 5 : return (pointValue / 1000).toFixed(1) + 'K';
                case 6 : return (pointValue / 1000).toFixed(0) + 'K';
                case 7 : return (pointValue / 1000000).toFixed(2) + 'M';
                case 8 : return (pointValue / 1000000).toFixed(1) + 'M';
                case 9 : return (pointValue / 1000000).toFixed(0) + 'M';
                case 10 : return (pointValue / 1000000000).toFixed(2) + 'B';
                case 11 : return (pointValue / 1000000000).toFixed(1) + 'B';
                case 12 : return (pointValue / 1000000000).toFixed(0) + 'B';
                default : return Utils.parsePointValue((pointValue / 1000000000).toFixed(0)) + 'B';
            }
        }
    }

    static hexToRgbPrefix(hex){
        let color = hex.substring(1).split('');
        if (color.length === 3) {
            color = [color[0], color[0], color[1], color[1], color[2], color[2]];
        }
        color = '0x' + color.join('');
        return 'rgba('+[(color >> 16) & 255, (color >> 8) & 255, color & 255].join(',') + ',';
    }

    static parsePointIndex(timelineStart, timelineStep, pointIndex, opt_includeYear) {
        //TODO: refactor
        let date = new Date(timelineStart + pointIndex * timelineStep);
        let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

        let label;

        if (timelineStep >= 86400000) {
            label = date.getDate() + ' ' + months[date.getMonth()];

            if (opt_includeYear) {
                label += (' ' + date.getFullYear());
            }
        } else {
            let minutes = date.getMinutes();
            let hours = date.getHours();
            label =  + (hours < 10 ? '0' : '') + hours + ':' + (minutes < 10 ? '0' : '') + minutes;

            if (opt_includeYear) {
                label += (', ' + date.getDate() + ' ' + months[date.getMonth()]);
            }
        }

        return label;
    }
}
