import {Chart, ChartType} from "./basic/Chart";
import {ViewPreview} from "./views/ViewPreview";
import {RangeControl} from "./RangeControl";
import {SummaryDrawable} from "./drawables/SummaryDrawable";

export class ApplicationInstance {
    constructor(root, chartData, drawHorizontalRulers = true) {
        this.root_ = root;
        let chart = new Chart(chartData);
        this.chart = chart;

        this.viewPreview_ = new ViewPreview(
            root.querySelector('.charts-preview canvas'),
            drawHorizontalRulers
        );

        this.viewPreview_.model_.chart = chart;
        this.rangeControl_ = new RangeControl(
            this.viewPreview_.model_,
            root.querySelector('.charts-range')
        );

        this.viewPreview_.setChart(chart, this.rangeControl_.rangeFrom, this.rangeControl_.rangeTo);

        root.classList.add('charts-initialized');
    }

    resize() {
        this.viewPreview_.resize();
        this.rangeControl_.resize();
    }

    setOffsets(topOffset, rulersOffsets, bottomOffset = null) {
      this.viewPreview_.setOffsets(topOffset, rulersOffsets, bottomOffset);
    }

    redraw(timePastInMs) {
        this.viewPreview_.setRange(this.rangeControl_.rangeFrom, this.rangeControl_.rangeTo);

        this.viewPreview_.draw(timePastInMs);
    }

    themeChange(themeAnimationPercent){
        this.viewPreview_.themeChange(themeAnimationPercent);
    }

    setRange(rangeFrom, rangeTo) {
      this.rangeControl_.setRange(rangeFrom, rangeTo);
    }

    getRange() {
      return [this.rangeControl_.rangeFrom, this.rangeControl_.rangeTo];
    }
}
