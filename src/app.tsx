import * as React from 'react';
import './assets/styles/common.scss';
import CoinPage from './pages/coin-page';

const App = () => <CoinPage />;

export default App;
