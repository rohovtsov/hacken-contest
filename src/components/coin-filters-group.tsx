import * as React from 'react';
import styles from './coin-filters-group.module.scss';
import {
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextField,
} from '@mui/material';
import { DesktopDatePicker } from '@mui/x-date-pickers/DesktopDatePicker';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { Dayjs } from 'dayjs';
import * as dayjs from 'dayjs';

interface CoinFiltersGroupProps {
  className?: string;
  date: string | null;
  setDate: (arg: string | null) => void;
  coin: string | null;
  setCoin: (arg: string | null) => void;
}

export default function CoinFiltersGroup({
  className,
  date,
  setDate,
  coin,
  setCoin,
}: CoinFiltersGroupProps) {
  return (
    <div className={styles['filters'] + ' ' + (className ?? '')}>
      <FormControl className={styles['coin-select']}>
        <InputLabel id="coin-select">Coin</InputLabel>
        <Select
          labelId="coin-select"
          id="coin-select"
          label="Coin"
          value={coin}
          onChange={(event) => setCoin(event.target?.value ?? null)}
        >
          <MenuItem value="bitcoin">Bitcoin</MenuItem>
          <MenuItem value="ethereum">Ethereum</MenuItem>
          <MenuItem value="litecoin">Litecoin</MenuItem>
        </Select>
      </FormControl>
      <LocalizationProvider dateAdapter={AdapterDayjs}>
        <DesktopDatePicker
          className={styles['date-select']}
          label="Date"
          inputFormat="DD/MM/YYYY"
          value={parseDateString(date)}
          onChange={(event) => setDate(parseDate(event))}
          renderInput={(params) => <TextField {...params} />}
          maxDate={dayjs(new Date())}
          minDate={dayjs(new Date(2000, 0, 1))}
        />
      </LocalizationProvider>
    </div>
  );
}

function parseDate(date: Dayjs): string {
  const d = date.date();
  const m = date.month() + 1;
  const y = date.year();
  return `${d < 10 ? '0' : ''}${d}-${m < 10 ? '0' : ''}${m}-${y}`;
}

function parseDateString(date: string): Dayjs {
  const [d, m, y] = date.split('-').map((i) => Number(i));
  return dayjs(new Date(y, m - 1, d).toDateString());
}
