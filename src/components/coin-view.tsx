import * as React from 'react';
import styles from './coin-view.module.scss';
import { CoinStats } from '../entities';

const CURRENCY = new Intl.NumberFormat('en-US', {
  style: 'currency',
  currency: 'USD',
});

interface CoinViewProps {
  date: string;
  stats: CoinStats;
}

export default function CoinView({ stats, date }: CoinViewProps) {
  const isStatsVisible =
    stats?.market_data?.current_price?.usd &&
    stats?.market_data?.market_cap?.usd &&
    stats?.market_data?.total_volume?.usd;

  return (
    <div className={styles['coin-view']}>
      <div className={styles['coin-view-logo']}>
        <img
          src={
            stats?.image?.large ?? stats?.image?.small ?? stats?.image?.thumb
          }
          alt={`${stats?.name} icon`}
        />
      </div>
      <div className={styles['coin-view-content']}>
        <h1>{stats?.name}</h1>
        <p>
          Ticker: <strong>{stats?.symbol?.toUpperCase()} / USD</strong>
        </p>
      </div>
      <div className={styles['space']}></div>
      {isStatsVisible ? (
        <div className={styles['coin-view-stats']}>
          <h3>Coin metrics at {date}</h3>
          <div className={styles['stats-wrap']}>
            <div className={styles['stats-item']}>
              <small>Price</small>
              <strong>
                {CURRENCY.format(stats?.market_data?.current_price?.usd)}
              </strong>
            </div>
            <div className={styles['stats-item']}>
              <small>Market Cap</small>
              <strong>
                {CURRENCY.format(stats?.market_data?.market_cap?.usd)}
              </strong>
            </div>
            <div className={styles['stats-item']}>
              <small>Total Volume</small>
              <strong>
                {CURRENCY.format(stats?.market_data?.total_volume?.usd)}
              </strong>
            </div>
          </div>
        </div>
      ) : null}
    </div>
  );
}
