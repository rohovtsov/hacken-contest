import * as React from 'react';
import styles from './error-view.module.scss';
import { Button } from '@mui/material';

interface ErrorViewProps {
  title?: string;
  type: 'fatal' | 'notice';
  onRetry?: () => void;
  children?: any;
}

export default function ErrorView({
  title,
  type,
  onRetry,
  children,
}: ErrorViewProps) {
  return (
    <div className={`${styles['error-view']} ${styles['type-' + type]}`}>
      <div className={styles['error-view-content']}>
        {title && <h3>{title}</h3>}
        {children}
      </div>
      {onRetry ? (
        <Button onClick={() => onRetry()} size="small" variant="outlined">
          Retry
        </Button>
      ) : null}
    </div>
  );
}
