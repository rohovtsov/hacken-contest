import * as React from 'react';
import styles from './coin-chart-view.module.scss';
import { useEffect } from 'react';
import { ApplicationInstance } from './../libs/telegram-chart/ApplicationInstance';
import { getChartData } from './../libs/telegram-chart/Data';
import { CoinChart } from '../entities';

interface CoinChartViewProps {
  chart: CoinChart;
}

export default function CoinChartView({ chart }: CoinChartViewProps) {
  useEffect(() => {
    const canvas = document.querySelector(`.${'charts-application'}`);
    const app = new ApplicationInstance(
      canvas,
      getChartData(
        'line',
        chart.prices.map((el) => el[0]),
        [chart.prices.map((el) => el[1])],
      ),
    );
    app.setRange(0, 1);

    const intervalId = setInterval(() => {
      app.redraw(16);
    }, 16);

    const resizeListener = () => {
      app.resize();
    };

    window.addEventListener('resize', resizeListener);

    return () => {
      clearInterval(intervalId);
      window.removeEventListener('resize', resizeListener);
    };
  });

  return (
    <div className={`${styles['coin-chart']} charts-application`}>
      <div className={`${styles['charts-preview']} charts-preview`}>
        <canvas></canvas>
      </div>
      <div className="chart-controls">
        <div className="chart-control">
          <div className="chart-control-inner"></div>
        </div>
      </div>
    </div>
  );
}
