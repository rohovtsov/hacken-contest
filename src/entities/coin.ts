export interface CoinStats {
  id: string;
  name: string;
  symbol: string;
  image: {
    thumb?: string;
    small?: string;
    large?: string;
  };
  market_data: {
    current_price: {
      usd: number;
    };
    market_cap: {
      usd: number;
    };
    total_volume: {
      usd: number;
    };
  };
}

export interface CoinChart {
  prices: [number, number][];
}
