import { CoinChart, CoinStats } from '../entities';
import { useEffect, useState } from 'react';
import { Api } from '../core';

export interface CoinPageState {
  stats?: CoinStats | null;
  chart?: CoinChart | null;
  error?: string | null;
  date: string;
  coin: string;
}

export function useCoinPageFacade(
  date: string,
  coin: string,
): [CoinPageState, (arg: string) => void, (arg: string) => void, () => void] {
  const [state, setState] = useState<CoinPageState>({ date, coin });

  useEffect(() => {
    load(coin, date);
  }, []);

  function load(coin, date) {
    const [d, m, y] = date.split('-').map(Number);
    const fromDate = Math.floor(new Date(y, m - 1, d).getTime() / 1000);
    const toDate = fromDate + 86400;

    setState((state) => ({
      ...state,
      coin,
      error: null,
      chart: null,
      stats: null,
    }));

    Promise.all([
      Api.getCoinStats(coin),
      Api.getCoinStatsForDate(coin, date),
      Api.getCoinChart(coin, fromDate, toDate),
    ])
      .then(([stats, statsForDate, chart]) => {
        const finalStats = {
          ...statsForDate,
          image: {
            ...(statsForDate?.image ?? {}),
            ...(stats?.image ?? {}),
          },
        };

        setState((state) => ({
          ...state,
          coin,
          date,
          chart,
          stats: finalStats,
        }));
      })
      .catch((err) => {
        setState((state) => ({
          ...state,
          coin,
          date,
          error: err.message,
        }));
      });
  }

  function setCoin(coin) {
    load(coin, state.date);
  }

  function setDate(date) {
    load(state.coin, date);
  }

  function onRetry() {
    load(state.coin, state.date);
  }

  return [state, setCoin, setDate, onRetry];
}
