import * as React from 'react';
import styles from './coin-page.module.scss';
import { useCoinPageFacade } from '../facades';
import { parseDate } from '../entities';
import CoinFiltersGroup from '../components/coin-filters-group';
import CoinView from '../components/coin-view';
import CoinChartView from '../components/coin-chart-view';
import ErrorView from '../components/error-view';

export default function CoinPage() {
  const [state, setCoin, setDate, onRetry] = useCoinPageFacade(
    parseDate(new Date()),
    'bitcoin',
  );
  const { date, coin, stats, chart, error } = state;

  return (stats && chart) || error ? (
    <div className={styles['page-inner'] + ' slide-animation'}>
      <div className={styles['page-inner-container'] + ' container'}>
        {stats ? <CoinView date={date} stats={stats} /> : null}

        <CoinFiltersGroup
          className={styles['filters-group']}
          coin={coin}
          date={date}
          setDate={setDate}
          setCoin={setCoin}
        />

        <div className={styles['content']}>
          {(chart?.prices?.length ?? 0) > 1 ? (
            <CoinChartView chart={chart} />
          ) : error ? (
            <ErrorView title="Error occurred" type="notice" onRetry={onRetry}>
              <p>Try changing filter parameters, or retry later. {error}</p>
            </ErrorView>
          ) : (
            <ErrorView title="Chart is unavailable" type="notice">
              <p>
                Try changing filter parameters, or retry later. Data granularity
                is automatic (cannot be adjusted)
              </p>
              <ul>
                <li>1 day from current time = 5 minute interval data</li>
                <li>1 - 90 days from current time = hourly data</li>
                <li>
                  above 90 days from current time = daily data (00:00 UTC)
                </li>
              </ul>
            </ErrorView>
          )}
        </div>
      </div>
    </div>
  ) : null;
}
